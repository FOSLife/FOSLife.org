# FOSLife.org

This collaborative website project aims to provide news and updates on projects that fulfill the values of free-and-open-source communities.

## Free & Open Source Software
* Debian: https://www.debian.org/

## Free & Open Source Hardware
* Computers
    * Any RISC-V projects to be listed here?

## Free & Open Source Communications
* Networking
* Communication Services
    * Internet Relay Chat (IRC)
    * Matrix
        * Dendrite (server): https://github.com/matrix-org/dendrite
* Amateur Radio
    * JS8Call: http://js8call.com/
    * CloudLog (cloud-based logging): https://github.com/magicbug/Cloudlog

## Free & Open Source Science/Education
* Medicine
    * COVID-19 vaccine sequences: https://github.com/NAalytics/Assemblies-of-putative-SARS-CoV2-spike-encoding-mRNA-sequences-for-vaccines-BNT-162b2-and-mRNA-1273

## Free & Open Source Media/Culture
* Books: https://standardebooks.org/
* Creative Commons: https://creativecommons.org/
* Open Galleries/Libraries/Archives/Museums (OpenGLAM): https://medium.com/open-glam

## Free & Open Source Physical Security
* Security cameras
    * ZoneMinder: https://zoneminder.com/ and https://github.com/ZoneMinder/zoneminder
* Firearms